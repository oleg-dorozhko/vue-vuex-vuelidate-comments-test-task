import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import { createStore } from './store'

const store = createStore()

//Vue.use(window.vuelidate.default)
Vue.use(Vuelidate)

new Vue({
  
   el: '#app', 
   
   store,
    
  render: h => h(App),
  
  methods: {
  teleport_to(target) {
		
				this.$store.commit('show_main',false);
				this.$store.commit('show_comments',false);
				this.$store.commit('view_comment',false);
				this.$store.commit('edit_comment',false);
				
				if(target === 'COMMENTS') {
									this.$store.commit('show_comments',true);
				}
				else if(target === 'ABOUT US') {
									this.$store.commit('show_main',true);
									 
				}
				else if(target === 'VIEW COMMENT') {
									this.$store.commit('view_comment',true);
									 
				}
				else if(target === 'EDIT COMMENT') {
									this.$store.commit('edit_comment',true);
									 
				}
				
				else if (target ==='WRITE_COMMENT_FORM') {
					this.$store.commit('show_main',true);
					 
					
				}
				
				
				
  }		}		
  
  
})


	
 
	
