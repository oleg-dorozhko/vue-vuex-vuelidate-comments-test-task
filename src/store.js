import Vue from 'vue'
import Vuex from 'vuex'
//import Vuelidate from 'vuelidate'
import axios from 'axios'

Vue.use(Vuex)
//Vue.use(Vuelidate)

export function createStore () {

	return new Vuex.Store({
		
		  state: {
			
			count: 0,
			item:{},
			items: [],
			loading: false,
			submitStatus: null,
			title: '',
			body:'',
			edit_comment: false,
			view_comment: false,
			show_comments: false,
			show_main: true,
			selected: null,
		
			
		  },
		  
		  mutations: {
			  
			UPDATE_SELECTED:   ( state, selected ) => {
   
				state.selected = selected;

			},
			  
			show_comments ( state, value )   {
				state.show_comments = value; 
			},	

			show_main ( state, value )   {
				state.show_main = value; 
			},			
			
			view_comment ( state, value )   {
				state.view_comment = value; 
			},
			  
			edit_comment ( state, value )   {
				state.edit_comment = value; 
			},
			  
			increment (state,step) {
			  state.count+=step
			},
			
			 UPDATE_ITEM: (state, item) => {
   
				state.item = item;

			},
			
			UPDATE_ITEMS: (state, items) => {
		  
			  state.items = items;

			}, 
			
			UPDATE_TITLE: (state, title) => {
				
				state.title = title;
				
			}, 
			
			SET_BODY: (state, body) => {
			  state.body = body;
			}, 
			
			SET_SUBMIT_STATUS: (state, submitStatus) => {
				state.submitStatus = submitStatus;
			},
			
			
			SET_LOADING: (state, loading) => {
			  state.loading = loading;
			}, 
	
		 },
		 
		 
		 
  getters: {
	  
	  selected(state) { return state.selected },
	  
	  show_main(state) { return state.show_main },
	  show_comments(state) { return state.show_comments },
	  view_comment(state) { return state.view_comment },
	  edit_comment(state) { return state.edit_comment },
	  submitStatus(state){ return state.submitStatus },
	   getItemById: state => id => {
    return state.items.find( function(itm){ 
		if(itm.id === id) 
			return true;
		return false;
	});
  },
  
  item(state) {
	return state.item
  },
	  
    items(state) {
		
		return state.items;
		
      // return state.items.map(t => {
        // return {
          // ...t,
         // }
      // })
    },
	
	 // $v (state) {      // don't expose api
        // return Object.assign({}, validator.$v)
      // }
  },
  
  actions: {
	  
	  
    loadItems({commit}) {
		
      commit('SET_LOADING', true);
	  
     	  return new Promise( (resolve, reject) => {
   
	 	
		axios.get('https://5cbef81d06a6810014c66193.mockapi.io/api/comments').then (  function(response) {
			
			console.log("get_comments: after getting "+response.data);
			response=response.data;
			
			let arr = [];

			for(let i=1;i<response.length;i+=2){
				arr.push([response[i],response[i-1]]);
			}	
			
			console.log(arr);	
			commit('SET_LOADING', false);
			resolve(arr);
			
		} )
	  
	  
	 
    
	}) .then((itemsParsed) => {
		  console.log(itemsParsed)
          commit('UPDATE_ITEMS', itemsParsed)
          commit('SET_LOADING', false)
        })
       
    },
	
	
	removeCommentById({commit}, id){
	
		commit('SET_LOADING', true);
		
		
		 return new Promise( (resolve, reject ) => {
			
			console.log("In store.js removeCommentById() ");		
			 console.log(id);
			 
								
					
			axios.delete(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments/`+id)
	  
			.then(response => {
				
				console.log("In store.js removeCommentById() response after removing post:")
				console.log(response);
			
			resolve(response);
			
			
			
			
		})
        .catch(e => {
          console.log(e);
        })

		 }) 
	  
	},
	
	
	updateForm({commit},params0){
		 commit('SET_LOADING', true);
	  
	  
     	  return new Promise( (resolve, reject ) => {
			    
		 console.log(params0);
			  let dtime = new Date().getTime();	
			  
			    const params = new URLSearchParams();
				params.append('created_at', dtime);
				params.append('title', params0.title);
				params.append('body', params0.body);
				console.log("id: "+params0.id);
				console.log("title: "+params0.title);
				console.log("body: "+params0.body);


        axios.put(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments/`+params0.id, params )
        .then(response => {
			
			console.log("In store.js updateForm() response after post:\n");
			console.log(response);
			//alert("response after post:\n" + response);
			//location.href="comments";
			commit('SET_LOADING', false);
			resolve(response);
		})
        .catch(e => {
          console.log(e);
		  commit('SET_LOADING', false);
		  reject(e);
        })
    
			  
			  
			  
		  })
   
		
	},
	
	
	sendForm({commit},params0){
		 commit('SET_LOADING', true);
	  
	  
     	  return new Promise( (resolve, reject ) => {
			    
		 console.log(params0);
			  let dtime = new Date().getTime();	
			  
			    const params = new URLSearchParams();
				params.append('created_at', dtime);
				params.append('title', params0.store.state.title);
				params.append('body', params0.store.state.body);


        axios.post(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments`, params  )
        .then(response => {
			
			console.log("In store.js sendForm() response after post:\n");
			console.log(response);
			//alert("response after post:\n" + response);
			//location.href="comments";
			commit('SET_LOADING', false);
			resolve(response);
		})
        .catch(e => {
          console.log(e);
		  commit('SET_LOADING', false);
		  reject(e);
        })
    
			  
			  
			  
		  })
   
		
	},
	
	setTitle: ( {commit} , title  )  => {
		
		commit('UPDATE_TITLE',title)
		
	},
	
	
	setBody: ( {commit} , body  )  => {
		
		commit( 'UPDATE_BODY', body )
		
	},	
	
	
	loadItem({commit}) {
		
		
		let selected=this.state.selected;
		
		if(selected === null) return null;
		console.log("Getting selected "+(selected));
		
      commit('SET_LOADING', true);
	  
      return new Promise((resolve, reject) => {
   
	 
		
		    axios.get('https://5cbef81d06a6810014c66193.mockapi.io/api/comments/'+selected)
			
			.then(   function(response) {
				
					console.log("In store.js loadItem(): " + response);
					response=response.data;
					//alert(response)
					resolve(response);
			
				 // $("#full_comment_caption").children().eq(0).html(response.title);
				 // $("#full_comment_text").html(response.body); //paragraph?
				  
				  // $(".comments_footer_row button").on("click", function(){
					 
				  // })
			
		     } )
			 
	}).then((itemParsed) => {
		
		  console.log(itemParsed)
          commit('UPDATE_ITEM', itemParsed)
          commit('SET_LOADING', false)
		  
        }).catch((err)=>{
			
			commit('SET_LOADING', false) //axelerator
		  	alert("err: "+err)	})
    },
	
	 
}, 
	
	
     
  	 
		 
	})
}
